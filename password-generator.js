const passwordGenerator = require('generate-password');

// Générer un mot de passe
const password = passwordGenerator.generate({
  length: 10,
  numbers: true
});

console.log('Mot de passe généré:', password);
