const nodemailer = require('nodemailer');

// Créer un transporteur pour envoyer l'email
let transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'votre.email@gmail.com',
    pass: 'votre_mot_de_passe'
  }
});

// Options de l'email
let mailOptions = {
  from: 'votre.email@gmail.com',
  to: 'destination.email@gmail.com',
  subject: 'Essai de Node.js',
  text: 'Bonjour, voici un e-mail envoyé depuis Node.js!'
};

// Envoyer l'email
transporter.sendMail(mailOptions, (error, info) => {
  if (error) {
    console.log('Erreur lors de l\'envoi de l\'email:', error);
  } else {
    console.log('Email envoyé:', info.response);
  }
});
